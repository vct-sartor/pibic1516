# Algoritimos de identificacao de grafos de Albuquerque (2012)

from utility import *


def vertices_de_interesse(A):
    '''
    Retorna uma lista com os vertices de interesse.
    '''

    n = A.nrows()
    V = []

    for i in range(n):
        if sum(A[i]) == n-2:
            V.append(i)

    if len(V) == 0:
        raise RuntimeError("impossible exception")

    return V


def has_special_format(A, v, sr = False):
    '''
    Retorna se o grafo G possui o formato especificado na conjectura
    de Albuquerque (2012) em relacao ao vertice v. A e a matriz de
    adjacencia de G.

    Se sr = True, a restricao de n2 <= n3 + 1 nao e considerada.
    '''

    n = A.nrows()

    # Precisamos encontrar quem e o vertice v1 nao adjacente ao
    # vertice de interesse v

    candidatos = range(n)
    candidatos.remove(v) # v nao e um candidato

    v1 = None
    for i in candidatos:
        if A[v,i] == 0:
            # Assim que encontrar um vertice que nao e vizinho,
            # salvar o seu indice e parar
            v1 = i
            break

    if v1 is None:
        raise RuntimeError("Could not find non-adjacent vertex")

    # Encontrar os vertices de V2 e V3

    # v1 nao e mais um candidato
    candidatos.remove(v1)

    V2 = []
    V3 = []
    for i in candidatos:
        if A[v1,i] == 0:
            V3.append(i)
        else:
            V2.append(i)

    # Verificar se n2 <= n3+1 quando sr=False
    if not sr and len(V2) > len(V3) + 1:
        return False

    # Verificar se todas as arestas existem
    for i in V2:
        for j in V3:
            if A[i,j] == 0:
                # Uma das arestas nao existe!
                return False

    return True
