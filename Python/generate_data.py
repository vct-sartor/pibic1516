from utility import *

import sys
import os


print 'Initializing script...'

# Pegar numero de vertices
print 'Select number of vertices to run:'
n = int(raw_input())
assert n >= 5 and n < 12, AssertionError('Number of vertices out of range')
print 'Running for n =', n, 'vertices.'

# Perguntar se quer pular a geracao de imagens (demorada)
skip_print = False
print 'Should skip the plotting phase? (y/n)'
skip_input = raw_input().strip()
assert skip_input in 'yn', AssertionError("Expected y or n")
if skip_input == 'y':
        skip_print = True

raw_data_path = '/home/victhor/pibic/Graphs/'
filtered_data_path = '/home/victhor/pibic/Graphs/'
final_data_path = '/home/victhor/pibic/Graphs/'
image_base_path = '/home/victhor/pibic/Plots/'
geng_path = 'nauty-geng'

# String processing
db_nauty = raw_data_path + 'n' + str(n) + '.g6'
geng_command = geng_path + ' -c ' + str(n) + ' >> ' + db_nauty
db_filtered = filtered_data_path + 'n' + str(n) + '_f.g6'
db_phi0 = final_data_path + 'n' + str(n) + '_phi0.tsv'
db_phi1 = final_data_path + 'n' + str(n) + '_phi1.tsv'
db_phid = final_data_path + 'n' + str(n) + '_phid.tsv'
img_phi0 = image_base_path + 'n' + str(n) + '_phi0/'
img_phi1 = image_base_path + 'n' + str(n) + '_phi1/'

# GENG generation step
print 'Starting GENG genration step...      Step: 1 / 4'

sys_call_result = os.system(geng_command)
if sys_call_result != 0:
        print 'Error during GENG generation step'
        sys.exit(1)

# Filtering step
print 'Starting filtering step...           Step: 2 / 4'

with open(db_nauty, 'r') as db_in, open(db_filtered, 'w') as db_out:
    for entry in db_in:
        graph = Graph(entry)
        # graph.degree_squence() ja e ordenado entao o primeiro valor
        # e garantido de ser o grau maximo
        if (graph.degree_sequence()[0] == n - 2):
            db_out.write(entry)

# Analysis step
print 'Starting function analysis step...   Step: 3 / 4'

with open(db_filtered, 'r') as db_in, open(db_phi0, 'w') as phi_0, \
     open(db_phi1, 'w') as phi_1, open(db_phid, 'w') as phi_d:

    for entry in db_in:
        entry = entry.rstrip()
        graph = Graph(entry)

        # Lista para lembrar subgrafos ja gerados e verificar se e isomorfo
        generated_subgraphs = []

        for vertex, degree in enumerate(graph.degree_iterator()):
            # Se o grau de v nao e n-2, ignorar e ir pro proximo
            if degree != n - 2:
                continue

            # E necessario criar uma copia, se nao delete o vertice do
            # proprio grafo
            subgraph = graph.copy()
            subgraph.delete_vertex(vertex)

            # Verficar se ja geramos um isomorfo
            skip = False
            for generated_subgraph in generated_subgraphs:
                if subgraph.is_isomorphic(generated_subgraph):
                    skip = True
                    break
            if skip:
                continue

            generated_subgraphs.append(subgraph.copy())

            phi = round(a(graph, 14) - a(subgraph, 14), 8)

            if phi == 0:
                phi_0.write(entry + '\t' + subgraph.graph6_string() + '\n')
            elif phi == 1:
                phi_1.write(entry + '\t' + subgraph.graph6_string() + '\n')
            else:
                phi_d.write(entry + '\t' + subgraph.graph6_string() +
                            '\t' + str(phi) + '\n')

# Image generation step
if not skip_print:
    print 'Starting image generation step...    Step: 4 / 4'

    with open(db_phi0, 'r') as db_in:
        i = 1
        for entry in db_in:
            g1, g2 = entry.split('\t')

            save_graph(Graph(g1), img_phi0 + str(i) + '_a')
            save_graph(Graph(g2), img_phi0 + str(i) + '_b')

            i = i + 1

    with open(db_phi1, 'r') as db_in:
        i = 1
        for entry in db_in:
            g1, g2 = entry.split('\t')

            save_graph(Graph(g1), img_phi1 + str(i) + '_a')
            save_graph(Graph(g2), img_phi1 + str(i) + '_b')

            i = i + 1
else:
    print 'Skipping image generation step.'

print 'Sucessfully finished execution.'
