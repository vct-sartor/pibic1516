# graph_pibic.py
# This file implements a few utility fuctions for doing analysis on
# the algebraic connectivity of graphs using the SAGE libraries.

from sage.all import *
from sage.repl.image import Image
import networkx


def save_graph(G, name = None, size = 4, show_fiedler = False):
    if name is None:
        name = 'Graph'
    name += '.png'

    P = G.plot(aspect_ratio = 1, figsize = [size, size])
    P += text('a(G) = ' + str(a(G)), (1, -1.5), aspect_ratio = 1.0,
              axes = False, figsize = [size, size], color = 'black')

    if show_fiedler:
        f = fiedler(G)
        of = []
        for n in f:
            if str(n)[-1] == '?':
                of.append(str(round(n, 6)) + '?')
            else:
                of.append(str(n))

            P += text('F = ' + str(of).translate(None, "'"), (1, -2), aspect_ratio = 1.0,
                      axes = False, figsize = [size, size], color = 'black')

    P.save(name, figsize=[size,size], aspect_ratio=1.0)


def a(G, precision = 6):
    value = None
    try:
        value = round(networkx.algebraic_connectivity(G.networkx_graph(), tol = 1e-10, method = 'lobpcg'), min(precision, 10))
    except:
        spectrum = G.spectrum(laplacian = True)
        value = round(spectrum[len(spectrum) - 2], precision)
    return value


def fiedler(G):
    tgt = a(G)
    for value, vectors, mult in G.eigenvectors(laplacian = True):
        if round(value, 6) == tgt:
            return vectors[0]
