# Script para analise dos grafos

from albuquerque import *

# Configuracao

n = 7
ignorar_restricao = False

arquivo_de_entrada = '/home/victhor/pibic/Graphs/n' + str(n) + '_f.g6'
arquivo_de_saida = '/home/victhor/pibic/Graphs/n' + str(n)
if ignorar_restricao:
    arquivo_de_saida += '_mx.tsv'
else:
    arquivo_de_saida += '_m.tsv'

f = open(arquivo_de_entrada, 'r')
o = open(arquivo_de_saida, 'w')

for graph6 in f:
    G = Graph(graph6)
    A = G.adjacency_matrix()

    V = vertices_de_interesse(A)
    S = []

    for v in V:
        # Se nao possui formato especial, continuar
        if (not has_special_format(A, v, ignorar_restricao)):
            continue

        # Subgrafo gerado pela remocao de v
        g = G.copy()
        g.delete_vertex(v)

        skip = False
        for s in S:
            if g.is_isomorphic(s):
                skip = True
                break
        if skip:
            continue

        S.append(g.copy())

        v = a(G, precision = 6)
        phi = round(a(G, precision = 8) - a(g, precision = 8), 6)

        # Salvar entrada
        o.write(graph6.rstrip() + '\t' + g.graph6_string() + '\t' + str(phi) +
                '\t' + str(v) + '\n')
